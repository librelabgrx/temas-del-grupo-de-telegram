# Temas del grupo de Telegram

Se trata de decidir de forma consensuada qué temas son admisibles en el grupo de Telegram y otros foros públicos que se creasen por parte de LibreLabGRX. Se busca evitar *ruido* y simplemente temas que no interesen al conjunto de las personas suscritas a este grupo. En general, el grupo está definido por el software, hardware, cultura y redes *libres*, es decir, con una licencia libre según alguna de las definiciones generalmente aceptadas de esos términos, que generalmente implican una licencia libre. Por lo tanto, temas aceptables son
* Dudas y problemas relacionados con el proceso de liberación de software, hardware y productos culturales.
* Noticias, dudas, problemas, difusión, peticiones de ayuda y discusiones relacionadas con aplicaciones, documentos o procesos específicos de software, hardware y cultura libre.
* Publicidad de eventos que incluyan en su totalidad o en una parte importante temas relacionados con lo anterior.
* Publicidad de puestos de trabajo, siempre que se trabaje con tecnologías libres de forma exclusiva o mayoritaria, y simpere que incluya todas las condiciones laborales y económicas del mismo.

Se excluirán explícitamente
* Hilos o discusiones que no tengan que ver con temas anteriores.
* "Trolling" o provocaciones que traten de enfrentar a diferentes productos de software libre.
* Incluso dentro de los temas anteriores, discurso de mal gusto u ofensivo en general o a algún colectivo en particular.
